import {Building} from './building.js';

export default class MetalPlant extends Building {
    static ID = 'metal_plant'

    constructor(buildingData) {
        super(buildingData.id)

        this.production = 3
        this.consumption = 3

        this.cost = {
            metal: 1000
        }
    }
}