import {Building} from './building.js';

export default class PowerPlant extends Building {
    static ID = 'power_plant'

    constructor(buildingData) {
        super(buildingData.id)

        this.production = 7
        this.consumption = 0

        this.cost = {
            metal: 1100
        }
    }
}