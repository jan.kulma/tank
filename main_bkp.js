import './style.css'

import * as PIXI from 'pixi.js'
import { Viewport } from 'pixi-viewport'

// import {Pixi} from 'pixi.js'
// window.Pixi = Pixi
import { Application, Graphics, Sprite, Assets, Text, Container} from 'pixi.js';
// import {PixiJSGrid} from 'pixijs-grid';

import {PixiJSGrid} from './grid.js'

// The application will create a renderer using WebGL, if possible,
// with a fallback to a canvas render. It will also setup the ticker
// and the root stage PIXI.Container
const app = new Application({autoDensity: true});

app.renderer.backgroundColor = 0x061639;
app.renderer.view.style.position = "absolute";
app.renderer.view.style.display = "block";
// // app.renderer.autoDensity = true;
app.resizeTo = window;

// scaleToWindow(app.renderer.view)

document.body.appendChild(app.view);

const tankTexture = await Assets.load('tank.png');
const tankSprite = new Sprite(tankTexture);

tankSprite.x = 64
tankSprite.y = 64
tankSprite.width = 64
tankSprite.height = 64
// tankSprite.anchor.x = 0.5;
// tankSprite.anchor.y = 0.5;
tankSprite.tint = 0x234234
// tankSprite.interactive = true
function moveTankToTarget() {
    if (!target) {
        return
    }

    // console.log('tank sprite x', tankSprite.x);
    // console.log('tank sprite y', tankSprite.y);
    const tankX = tankSprite.x / 64
    const tankY = tankSprite.y / 64
    // const targetX = convertCrd(target.x)
    // const targetY = convertCrd(target.y)
    const targetX = target.x
    const targetY = target.y

    // console.log({scale});
    // console.log({targetX}, {targetY});
    // console.log({tankX}, {tankY});
    //
    // return


    if (tankX === targetX && tankY === targetY) {
        target = null
        return
    }
    if (tankX !== targetX) {
        const xDir = tankX > targetX ? -1 : 1
        console.log({xDir});
        tankSprite.x += xDir * 64
    }
    if (tankY !== targetY) {
        const yDir = tankY > targetY ? -1 : 1
        console.log({yDir});
        tankSprite.y += yDir * 64
    }
}



// let skipFrame = false
function gameLoop(delta) {
    // if (skipFrame === true) {
    //     skipFrame = false
    //     return
    // }

    // console.log(delta);
    moveTankToTarget()

    // skipFrame = true
}

app.ticker.maxFPS = 30

app.ticker.add((delta) => gameLoop(delta));

app.stage.addChild(tankSprite);
// // Setup the position of the bunny
// bunny.x = app.renderer.width / 2;
// bunny.y = app.renderer.height / 2;
//
// // Rotate around the center
// bunny.anchor.x = 0.5;
// bunny.anchor.y = 0.5;
//
// // Add the bunny to the scene we are building
// app.stage.addChild(bunny);


const grid = new PixiJSGrid(1000, 64, {
    width: 1,
    color: 0xffffff,
    alpha: 0.25,
    alignment: 0.5,
    native: true,
}).drawGrid();

grid.interactive = true





app.stage.addChild(grid);





let attacker

let target
let x = 0
let y = 0

let scale = {
    x: 1,
    y: 1,
}

function convertCrd(x) {
    return Math.floor((x / scale.x) / 64)
}
function reverseConvertCrd(x) {
    return Math.floor((x / scale.x) * 64)
}


// app.renderer.plugins.interaction.onPointerDown = (e) => {
//     console.log(e);
// }

grid.on('mousedown', function(e) {

    // console.log('is this valid?', e.x - app.stage.x);
    //
    // var mousePosition = app.renderer.plugins.interaction;
    //
    //
    // console.log({mousePosition});

    // let x = Math.floor((e.x / scale.x) / 64)
    // let y = Math.floor((e.y / scale.y) / 64)
    let x = convertCrd(e.x - app.stage.x)
    let y = convertCrd(e.y - app.stage.y)

    console.log(e);
    // console.log(e.y);
    // console.log(x, y);

    // while (x % 64 !== 0) {
    //     x++
    // }
    // while (y % 64 !== 0) {
    //     y++
    // }
    //
    // x *= 2
    // y *= 2


    target = {
        x,
        y,
    }
});



// const grid = new PixiJSGrid(20).drawGrid();


const rectangle = new Graphics();
rectangle.lineStyle({width: 2, color: 0xFFFFFF, alpha: 0.1});
rectangle.lineTo(100, 100)
rectangle.endFill();

rectangle.x = 20
rectangle.y = 20
app.stage.addChild(rectangle)

for (let i = 0; i < 100; i++) {
    for (let j = 0; j < 100; j++) {
        continue
        const rectangle = new Graphics();
        rectangle.lineStyle({width: 2, color: 0xFFFFFF, alpha: 0.1});
        rectangle.beginFill(0xFFFFFF, 0.05);
        rectangle.drawRect(0, 0, 64, 64);
        rectangle.endFill();
        rectangle.cullable = true
        rectangle.x = x;
        rectangle.y = y;

        rectangle.interactive = true;
        rectangle.on("mousedown", (e) => {

            target = e.target
            return
            console.log(e.target.x);
            console.log(e.target.y);
            return
            if (!attacker) {
                e.target.clear();
                e.target.beginFill(0xFFFFFF, 0.25);
                e.target.lineStyle({width: 2, color: 0xFFFFFF, alpha: 0.1});
                e.target.drawRect(0, 0, 64, 64);
                e.target.endFill();

                attacker = e.target
                return
            }

            target = e.target

            const dx = attacker.x > target.x ? -1 : 1
            const dy = attacker.y > target.y ? -1 : 1

            function move({attacker, target}) {
                console.log(attacker, target);
                if (attacker.x !== target.x) {
                    attacker.x += dx * 64 / 8
                }
                if (attacker.y !== target.y) {
                    attacker.y += dy * 64 / 8
                }

                if (attacker.x === target.x && attacker.y === target.y) {
                    app.ticker.remove(move, attacker, target)
                }
            }

            app.ticker.add(move, {attacker, target});

            // attacker = null
            // target = null


            // console.log(e.target.x);
            // console.log(e.target.y);

            // const message = new Text("Hello Pixi!");
            // message.x = e.target.x
            // message.y = e.target.y
            // console.log(message);
            // app.stage.addChild(message)
            // message.position.set(e.target.x, e.target.y);



            // move(e.target, 4, 5)

            // console.log(e.target);
            //


        });

        // rectangle.on("pointerover", () => console.log('dsfsd'));
        // rectangle.on("pointerout", () => console.log('dsfsd'));

        app.stage.addChild(rectangle);
        x += 64
    }
    y += 64
    x = 0
}
// function move(x, y) {
//     this.x += x * 64 / 10
//     this.y += y * 64 / 10
//
//     // if (this.x )
// }






document.body.addEventListener("wheel", (e) => {
    // app.stage.scale.set(2, 2)


    // console.log(convertCrd((e.clientX / app.stage.scale.x) - app.stage.x) * 64);
    //
    // return
    // console.log(convertCrd((e.clientY * scale.y) - app.stage.y));

    // return

    // console.log(Math.floor(e.clientY / 64));
    // grid.pivot.set(
    //     convertCrd((e.clientX / app.stage.scale.x) - app.stage.x) * 64,
    //     convertCrd((e.clientY / app.stage.scale.y) - app.stage.y) * 64
    // )
    // console.log('grid pivot', grid.pivot.x, grid.pivot.y);


    // return
    const currentScale = app.stage.scale
    if (e.deltaY < 0) {
        currentScale.y += 0.02
        currentScale.x += 0.02
    } else {
        currentScale.y -= 0.02
        currentScale.x -= 0.02
    }

    if (currentScale.x < 0.2 || currentScale.y < 0.2) {
        currentScale.y = 0.2
        currentScale.x = 0.2
    }
    if (currentScale.x > 2 || currentScale.y > 2) {
        currentScale.y = 2
        currentScale.x = 2
    }



    // currentScale.y = +(currentScale.y.toFixed(1))
    // currentScale.x = +(currentScale.x.toFixed(1))

    console.log(currentScale.y);
    console.log(currentScale.x);

    app.stage.scale.set(currentScale.x, currentScale.y)

    scale = currentScale

    // console.log(e);
})


document.body.addEventListener('keydown', (e) => {
    switch (e.key) {
        case 'ArrowLeft':
            app.stage.x += 20
            break;
        case 'ArrowRight':
            app.stage.x -= 20
            break
        case 'ArrowUp':
            app.stage.y += 20
            break
        case 'ArrowDown':
            app.stage.y -= 20
            break
    }

    // app.stage.scale.set(2, 2)

    // console.log(e);stage.x = renderer.width/2 and stage.y = renderer.height/2

    // app.stage.x = app.stage.x - 10
    // app.stage.y = app.stage.y - 10
})

// The application will create a canvas element for you that you
// can then insert into the DOM
// document.body.appendChild(app.view);
//
// // load the texture we need
// const texture = await Assets.load('bunny.png');
//
// // This creates a texture from a 'bunny.png' image
// const bunny = new Sprite(texture);
//
// // Setup the position of the bunny
// bunny.x = app.renderer.width / 2;
// bunny.y = app.renderer.height / 2;
//
// // Rotate around the center
// bunny.anchor.x = 0.5;
// bunny.anchor.y = 0.5;
//
// // Add the bunny to the scene we are building
// app.stage.addChild(bunny);
//
// // Listen for frame updates
// app.ticker.add(() => {
//     // each frame we spin the bunny around a bit
//     bunny.rotation += 0.01;
// });

function scaleToWindow(canvas, backgroundColor) {
    var scaleX, scaleY, scale, center;

    //1. Scale the canvas to the correct size
    //Figure out the scale amount on each axis
    scaleX = window.innerWidth / canvas.offsetWidth;
    scaleY = window.innerHeight / canvas.offsetHeight;

    //Scale the canvas based on whichever value is less: `scaleX` or `scaleY`
    scale = Math.min(scaleX, scaleY);
    canvas.style.transformOrigin = "0 0";
    canvas.style.transform = "scale(" + scale + ")";

    //2. Center the canvas.
    //Decide whether to center the canvas vertically or horizontally.
    //Wide canvases should be centered vertically, and
    //square or tall canvases should be centered horizontally
    if (canvas.offsetWidth > canvas.offsetHeight) {
        if (canvas.offsetWidth * scale < window.innerWidth) {
            center = "horizontally";
        } else {
            center = "vertically";
        }
    } else {
        if (canvas.offsetHeight * scale < window.innerHeight) {
            center = "vertically";
        } else {
            center = "horizontally";
        }
    }

    //Center horizontally (for square or tall canvases)
    var margin;
    if (center === "horizontally") {
        margin = (window.innerWidth - canvas.offsetWidth * scale) / 2;
        canvas.style.marginTop = 0 + "px";
        canvas.style.marginBottom = 0 + "px";
        canvas.style.marginLeft = margin + "px";
        canvas.style.marginRight = margin + "px";
    }

    //Center vertically (for wide canvases)
    if (center === "vertically") {
        margin = (window.innerHeight - canvas.offsetHeight * scale) / 2;
        canvas.style.marginTop = margin + "px";
        canvas.style.marginBottom = margin + "px";
        canvas.style.marginLeft = 0 + "px";
        canvas.style.marginRight = 0 + "px";
    }

    //3. Remove any padding from the canvas  and body and set the canvas
    //display style to "block"
    canvas.style.paddingLeft = 0 + "px";
    canvas.style.paddingRight = 0 + "px";
    canvas.style.paddingTop = 0 + "px";
    canvas.style.paddingBottom = 0 + "px";
    canvas.style.display = "block";

    //4. Set the color of the HTML body background
    document.body.style.backgroundColor = backgroundColor;

    //Fix some quirkiness in scaling for Safari
    var ua = navigator.userAgent.toLowerCase();
    if (ua.indexOf("safari") != -1) {
        if (ua.indexOf("chrome") > -1) {
            // Chrome
        } else {
            // Safari
            //canvas.style.maxHeight = "100%";
            //canvas.style.minHeight = "100%";
        }
    }

    //5. Return the `scale` value. This is important, because you'll nee this value
    //for correct hit testing between the pointer and sprites
    return scale;
}