import State from './state.js';
import {Ticker} from 'pixi.js';
import * as PF from 'pathfinding'

// TODO: fix mouse selection when mose goes off grid
export default class MovementManager {

    stateMovements = []
    renderingMovements = []

    start

    constructor(grid) {
        this.grid = grid
        this.pathfinderGrid = new PF.Grid(State.boardWidth, State.boardWidth)
    }

    createPath(from, to) {
        const grid = this.pathfinderGrid.clone()
        for (const x in State.fields) {
            for (const y in State.fields[x]) {
                if (State.fields[x][y] && !State.fields[x][y].isMoving) {
                    grid.setWalkableAt(x, y, false)
                }
            }
        }

        const finder = new PF.AStarFinder()

        const pathfinderPath = finder.findPath(from.x, from.y, to.x, to.y, grid);

        console.log({pathfinderPath});

        const path = []

        // debugger

        for(let i = 1; i < pathfinderPath.length; i++) {
            const [prevX, prevY] = pathfinderPath[i -1]
            const [x, y] = pathfinderPath[i]

            if (prevX !== x) {
                path.push({axis: 'x', val: -1 * (prevX - x), adjusted: false})
            }
            if (prevY !== y) {
                path.push({axis: 'y', val: -1 * (prevY - y), adjusted: false})
            }
        }

        // path[0].adjusted = true
        // if (path[1]) {
        //     path[1].adjusted = true
        // }

        return path
    }

    createPath2(from, to) {
        const current = {x: from.x, y: from.y}
        const path = []

        while (current.x !== to.x || current.y !== to.y) {
            debugger

            if (current.x !== to.x) {
                const xDir = current.x > to.x ? -1 : 1
                const nextStep = {axis: '', val: 0, adjusted: false}

                if (!State.isFieldFree(current.x + xDir * 1, current.y)) {
                    const yDir = current.y > to.y ? -1 : 1

                    if (State.isFieldFree(current.x, current.y + yDir * 1)) {

                        nextStep.axis = 'y'
                        nextStep.val = yDir * 1
                        // current.y += 1 * 1
                        // path.push({axis: 'y', val: 1, adjusted: false})
                        // continue;
                    } else if (State.isFieldFree(current.x, current.y + -1 * yDir * 1)) {
                        nextStep.axis = 'y'
                        nextStep.val = -1 * yDir * 1

                        // current.y += -1 * 1
                        // path.push({axis: 'y', val: -1, adjusted: false})
                        // continue;

                    } else if (State.isFieldFree(current.x + -1 * xDir * 1, current.y)) {
                        nextStep.axis = 'x'
                        nextStep.val = -1 * xDir * 1


                        // current.x += -1 * xDir * 1 // replace by speed?
                        // path.push({axis: 'x', val: -1 * xDir, adjusted: false})
                    } else {
                        console.log(
                            'all fields busy when trying to move on x axis');
                    }
                } else {

                    nextStep.axis = 'x'
                    nextStep.val = xDir * 1

                    // current.x += xDir * 1 // replace by speed?
                    // path.push({axis: 'x', val: xDir, adjusted: false})
                }

                if (path.length && path[path.length - 1].axis === nextStep.axis && path[path.length - 1].val === nextStep.val * -1) {
                    console.log('skipping, would be step back on x axis');
                    continue
                } else {
                    current[nextStep.axis] += nextStep.val
                    path.push(nextStep)
                }
            }
            if (current.y !== to.y) {
                const yDir = current.y > to.y ? -1 : 1
                const nextStep = {axis: '', val: 0, adjusted: false}

                if (!State.isFieldFree(current.x, current.y + yDir * 1)) {
                    const xDir = current.x > to.x ? -1 : 1
                    if (State.isFieldFree(current.x + xDir * 1, current.y)) {
                        nextStep.axis = 'x'
                        nextStep.val = xDir * 1
                        // current.x += xDir * 1
                        // path.push({axis: 'x', val: xDir * 1, adjusted: false})
                        // continue;
                    } else if (State.isFieldFree(current.x + -1 * xDir * 1, current.y)) {
                        nextStep.axis = 'x'
                        nextStep.val = -1 * xDir * 1

                        // current.x += -1 * yDir * 1
                        // path.push({axis: 'x', val: -1 * yDir * 1, adjusted: false})
                        // continue;
                    } else if (State.isFieldFree(current.x, current.y + -1 * yDir * 1)) {
                        nextStep.axis = 'y'
                        nextStep.val = -1 * yDir * 1

                        // current.y += -1 * yDir * 1
                        // path.push({axis: 'y', val: -1 * yDir * 1, adjusted: false})
                    } else {
                        console.log(
                            'all fields busy wien trying to move on y axis');
                    }
                } else {
                    nextStep.axis = 'y'
                    nextStep.val = yDir * 1

                    // current.y += yDir * 1 // replace by speed?
                    // path.push({axis: 'y', val: yDir, adjusted: false})
                }

                if (path.length && path[path.length - 1].axis === nextStep.axis && path[path.length - 1].val === nextStep.val * -1) {
                    console.log('skipping, would be step back on y axis');
                } else {
                    current[nextStep.axis] += nextStep.val
                    path.push(nextStep)
                }

            }
        }

        path[0].adjusted = true
        if (path[1]) {
            path[1].adjusted = true
        }

        return path
    }

    createMovement(object, target) {
        const path = this.createPath({x: object.gridX, y: object.gridY}, {x: target.x, y: target.y});

        console.log(path);

        if (!path.length) {
            return
        }

        this.start = Date.now() / 1000

        this.addRenderingMovement({
            path,
            object
        })

        // this.updateRenderingMovements()

        let interval = 2000
        switch (object.speed) {
            case 1:
                break
            case 2:
                interval = 1000
                break
            case 3:
                interval = 500
                break
        }

        // interval = interval / 5

        // this.updateStateMovement({
        //     path,
        //     object
        // });

        // console.log(interval);

        const stateMoving = () => {
            setTimeout(() => {
                const stillMoving = this.updateStateMovement({
                    path,
                    object
                });

                if (stillMoving) {
                    stateMoving()
                }
            }, interval / 2)
            // setTimeout(() => {
            //     this.updateRendedrMovement({
            //         path,
            //         object
            //     });
            // }, interval / 60)
        }

        stateMoving()

        // const renderMoving = () => {
        //     setTimeout(() => {
        //         const stillMoving = this.updateRendedrMovement({
        //             path,
        //             object
        //         });
        //
        //         if (stillMoving) {
        //             renderMoving()
        //         }
        //     }, interval / 60)
        // }
        //
        // renderMoving()

        // this.stateMovements.push({
        //     path,
        //     object,
        //     start: Date.now() / 1000
        // })
    }

    updateStateMovement(movement) {
        if (!movement.path.length) {

            console.log(
                'state arrived',
                movement.object.gridX,
                movement.object.gridY,
                Date.now() / 1000
            );

            // console.log('STATE path empty, arrived at target, removing from movement');
            // console.log('S', new Date(), (new Date()).getMilliseconds())
            // console.log('s', (Date.now() / 1000) - this.start);

            return false
        }
        const object = movement.object

        const currentGridX = movement.object.gridX
        const currentGridY = movement.object.gridY

        this.adjustRenderings()

        const {axis, val} = movement.path.shift()

        if (!axis || (axis !== 'x' && axis !== 'y')) {
            throw 'axis cannot be undefined or have dif axis val then'
        }

        if (axis === 'x') {
            // if (State.isFieldFree(currentGridX + val, currentGridY)) {
                State.freeUpField(movement.object.gridX, movement.object.gridY)
                movement.object.setGridXY(currentGridX + val, currentGridY)
                State.putInField(currentGridX + val, currentGridY, movement.object)
            // }

        }
        if (axis === 'y') {
            // if (State.isFieldFree(currentGridX, currentGridY + val)) {
                State.freeUpField(movement.object.gridX, movement.object.gridY)
                movement.object.setGridXY(currentGridX, currentGridY + val)
                State.putInField(currentGridX, currentGridY + val, movement.object)
            // }
        }


        // movement.object.setGridXY(x, y)

        // console.log('s', x, Math.floor(Date.now() / 10));

        // State.putInField(x, y, movement.object)

        return true
    }

    updateRendedrMovement(movement) {

        const x = this.grid.convertX(movement.object.getBounds().x);
        if (this.lastX !== x) {
            this.lastX = x
            console.log('r', this.lastX, Math.floor(Date.now() / 100));
            console.log('');
            console.log('');
            console.log('');
        }
        // this.lastX = x;

        // const distX = ((movement.object.gridX * 64) - movement.object.x) / 60
        // const distY = ((movement.object.gridY * 64) - movement.object.y) / 60

        movement.object.x += 64 / 60
        // movement.object.y += distY

        // console.log(distX);
        // console.log(distY);

        return true
    }

    addRenderingMovement(movement) {
        this.renderingMovements.push(movement)
    }


    adjustRenderings() {
        // for (let i = 0; i < this.renderingMovements.length; i++) {
        //     const movement = this.renderingMovements[i]
        //
        //     console.log(
        //         'adjust renderings call',
        //         movement.object.x, '=>', Math.floor((movement.object.x / 64)) * 64,
        //         movement.object.y, '=>', Math.floor((movement.object.y / 64)) * 64
        //     );
        //     movement.object.x = Math.floor((movement.object.x / 64)) * 64
        //     movement.object.y = Math.floor((movement.object.y / 64)) * 64
        // }
    }

    updateRenderingMovements() {

        const delta = Ticker.shared.deltaTime

        // const distance = 64 / 60 / 2 // 0.5 field 1 sec
        // const distance = 64 / 60 // 1 field 1 sec
        // const distance = 64 / 60 * 1.5 // 1.5 field 1 sec
        // const distance = 64 / 60 * 2 // 2 field 1 sec

        for (let i = 0; i < this.renderingMovements.length; i++) {
            const movement = this.renderingMovements[i]

            // console.log(...movement.path);

            // const x = this.grid.convertX(movement.object.getBounds().x);
            // if (this.lastX !== x) {
            //     this.lastX = x
            //     console.log('r', this.lastX, Math.floor(Date.now() / 10));
            //     console.log('');
            //     console.log('');
            //     console.log('');
            // }
            // this.lastX = x;

            // const distX = ((movement.object.gridX * 64) - movement.object.x) / 60
            // const distY = ((movement.object.gridY * 64) - movement.object.y) / 60

            if (!movement.path.length) {
                console.log(
                    'rendering arrived',
                    this.grid.convertX(movement.object.getBounds().x),
                    this.grid.convertY(movement.object.getBounds().y),
                    this.grid.convertXToReal(movement.object.getBounds().x),
                    this.grid.convertYToReal(movement.object.getBounds().y),
                    Date.now() / 1000
                );

                // console.log(
                //     'last path ahead, adjuusting',
                //     movement.object.x, '=>', Math.floor((movement.object.x / 64)) * 64,
                //     movement.object.y, '=>', Math.floor((movement.object.y / 64)) * 64
                // );
                // movement.object.x = Math.floor((movement.object.x / 64)) * 64
                // movement.object.y = Math.floor((movement.object.y / 64)) * 64


                this.renderingMovements.splice(i, 1)


                continue
            }

            // console.log(Math.abs(movement.object.x - Math.floor((movement.object.x / 64)) * 64));

            // if (Math.abs(movement.object.x - Math.round((movement.object.x / 64)) * 64) > 64) {
            //     console.log('adjusting x');
            //     movement.object.x = Math.floor((movement.object.x / 64)) * 64
            // }
            // if (Math.abs(movement.object.y - Math.floor((movement.object.y / 64)) * 64) > 64) {
            //     console.log('adjusting y');
            //     movement.object.y = Math.floor((movement.object.y / 64)) * 64
            // }


            // if (movement.path.length % 10 === 0) {
            //     console.log(
            //         'adjuusting',
            //         movement.object.x, '=>', Math.floor((movement.object.x / 64)) * 64,
            //         movement.object.y, '=>', Math.floor((movement.object.y / 64)) * 64
            //     );
            //     movement.object.x = Math.floor((movement.object.x / 64)) * 64
            //     movement.object.y = Math.floor((movement.object.y / 64)) * 64
            // }

            const {axis, val, adjusted} = movement.path[0]



            if (axis === 'x') {

                if (!adjusted) {
                    movement.object.x = (Math.round((movement.object.x / 64))) * 64
                    movement.path[0].adjusted = true
                }

                movement.object.texture.rotate = val < 0 ? 2 : 6
                movement.object.x += ((64 * val * 2) / 60) * delta
            }
            if (axis === 'y') {


                if (!adjusted) {
                    movement.object.y = (Math.round((movement.object.y / 64))) * 64
                    movement.path[0].adjusted = true
                }
                movement.object.texture.rotate = val < 0 ? 0 : 4
                movement.object.y += ((64 * val * 2) / 60) * delta
            }

            // if (!adjusted) {
            //     console.log(
            //         'adj false, adjuusting',
            //         movement.object.x, '=>', Math.floor((movement.object.x / 64)) * 64,
            //         movement.object.y, '=>', Math.floor((movement.object.y / 64)) * 64
            //     );
            //     movement.object.x = Math.floor((movement.object.x / 64)) * 64
            //     movement.object.y = Math.floor((movement.object.y / 64)) * 64
            //
            //     movement.path[0].adjusted = true
            // } else {
            //     // console.log('adjusted');
            // }

            // if (movement.path.length === 1) {







            // }

            // return


            // const distX = ((movement.object.gridX * 64) - movement.object.x) / 120
            // const distY = ((movement.object.gridY * 64) - movement.object.y) / 120
            //
            // movement.object.x += distX * delta
            // movement.object.y += distY
            //
            // return
            //
            //
            //
            //
            //
            //
            //
            // if (!movement.path.length) {
            //     console.log('RENDERING path empty, arrived at target, removing from movement');
            //     console.log('R', new Date(), (new Date()).getMilliseconds())
            //     console.log('r', (Date.now() / 1000) - this.start);
            //     this.renderingMovements.splice(i, 1)
            //     continue
            // }
            //
            // const object = movement.object
            // const speed = object.speed
            //
            // let distance = 64 / 60
            //
            // switch (speed) {
            //     case 1:
            //         distance = distance / 2
            //         break
            //     case 2:
            //         break
            //     case 3:
            //         distance = distance * 2
            //         break
            //     case 4:
            //         distance = distance * 8
            //         break
            // }
            //
            // // distance = distance * 5
            //
            // distance = distance * delta
            //
            // distance = Number(distance.toFixed(0))
            //
            // // console.log(distance);
            //
            // const {x, y} = movement.path[0]
            // const to = {
            //     x: x * 64,
            //     y: y * 64,
            // }
            //
            //
            // const objGridX = object.gridX
            // const objGridX2 = this.grid.convertX(object.getBounds().x);
            // const objGridY = object.gridY
            // const objGridY2 = this.grid.convertY(object.getBounds().y);
            //
            // console.log({objGridX}, {objGridX2});
            // console.log({objGridY}, {objGridY2});
            //
            // // if (x === objGridX && y === objGridY) {
            // //     console.log(movement.path);
            // // }
            //
            // if (x !== objGridX) {
            //
            //     const xDir = object.x > to.x ? -1 : 1
            //
            //     if ((xDir > 0 && object.x < to.x) || (xDir < 0 && object.x > to.x)) {
            //
            //         object.texture.rotate = xDir < 0 ? 2 : 6
            //
            //
            //         object.x += xDir * distance
            //
            //         if ((xDir > 0 && object.x >= to.x) || (xDir < 0 && object.x <= to.x)) {
            //             console.log('i vener do this');
            //             // object.x = to.x
            //             movement.path.shift()
            //         }
            //     }
            //
            //     // continue
            //
            // }
            //
            // if (y !== objGridY) {
            //     const yDir = object.y > to.y ? -1 : 1
            //
            //
            //
            //
            //
            //     if ((yDir > 0 && object.y < to.y) || (yDir < 0 && object.y > to.y)) {
            //
            //         // if (object.y !== to.y) {
            //
            //         object.texture.rotate = yDir < 0 ? 0 : 4
            //
            //         object.y += yDir * distance
            //         if ((yDir > 0 && object.y >= to.y) || (yDir < 0 && object.y <= to.y)) {
            //             console.log('i vener do this');
            //
            //             // object.y = to.y
            //             movement.path.shift()
            //         }
            //     }
            // }

        }

        // requestAnimationFrame(this.updateRenderingMovements.bind(this))
    }
}