import {Sprite} from 'pixi.js'
import {AssetsManager} from './assetsManager.js';
import ContextInterfaceManager from './contextInterfaceManager.js';

export class Building extends Sprite {

    built = false

    constructor(buildingId) {
        super(AssetsManager.textures[buildingId])

        // custom
        this.id = buildingId
        this.uid = Math.floor(Math.random() * Math.floor(Math.random() * Date.now()))

        // inherited
        this.alpha = 0.5;
        this.width = this.height = 64;
        this.visible = false;
        this.interactive = true

        return this;
    }

    finishBuilding() {
        this.built = true
        this.alpha = 0.8
        this.contextInterfaceManager = new ContextInterfaceManager(this)
    }

    actions() {
        return []
    }
}