import {PixiJSGrid} from './grid.js'
import State from '/state.js'
import MovementManager from './movementManager.js';

export class GameGrid extends PixiJSGrid {
    constructor(viewport) {
        super(
            64 * State.boardWidth,
            64,
            {
                width: 1,
                color: 0xffffff,
                alpha: 0.05,
                alignment: 0.5,
                native: true,
            },
            false
        )

        this.interactive = true
        this.drawGrid()

        this.viewport = viewport

        // this.registerSelectionEvents()
    }

    convertX(x) {
        return Math.floor((x - this.viewport.x) / this.viewport.scale.x / 64)
    }

    convertY(y) {
        return Math.floor((y - this.viewport.y) / this.viewport.scale.y / 64)
    }

    convertXToRealRounded(x) {
        return this.convertX(x) * 64
    }

    convertYToRealRounded(y) {
        return this.convertY(y) * 64
    }

    convertXToReal(x) {
        return Math.floor((x - this.viewport.x) / this.viewport.scale.x)
    }

    convertYToReal(y) {
        return Math.floor((y - this.viewport.y) / this.viewport.scale.y)
    }

    registerSelectionEvents() {

        let selection = {
            start: null,
            end: null,
        }

        const selectionDiv = document.createElement('div')
        selectionDiv.style.display = 'none'
        selectionDiv.classList.add(
            'fixed',
            'z-50',
            'border-2',
            'border-white'
        )
        document.body.appendChild(selectionDiv)

        const mouseDownHandler = (e) => {
            selection.start = {
                x: e.global.x,
                y: e.global.y,
            }

            this.on('mousemove', mouseMoveHandler)
            this.on('mouseup', mouseUpHandler)
        }
        const mouseMoveHandler = (e) => {
            selection.end = {
                x: e.global.x,
                y: e.global.y,
            }
            drawSelection()
        }
        const mouseUpHandler = (e) => {



            convertSelectionToGridCoordinated(selection);

            this.off('mousedown', mouseDownHandler)
            this.off('mousemove', mouseMoveHandler)
            this.off('mouseup', mouseUpHandler)
            selectionDiv.style.display = 'none'
            selectionDiv.style.left = ''
            selectionDiv.style.top = ''
            selectionDiv.style.width = ''
            selectionDiv.style.height = ''

            if (State.selectionContext.length) {
                this.on('mousedown', selectTargetHandler)
            } else {
                this.on('mousedown', mouseDownHandler)
            }



            console.log('now i switch');

        }

        this.on('mousedown', mouseDownHandler)

        const selectTargetHandler = (e) => {
            let x = this.convertX(e.x)
            let y = this.convertY(e.y)

            const movementManager = new MovementManager(this)


            State.selectionContext.forEach((object) => {
                console.log('lul');
                movementManager.createMovement(
                    object,
                    {x, y}
                )
            })


            State.selectionContext = []
            this.off('mousedown', selectTargetHandler)
            this.on('mousedown', mouseDownHandler)
        }

        const drawSelection = () => {
            selectionDiv.style.display = 'block'
            if (selection.start.x < selection.end.x) {
                selectionDiv.style.left = selection.start.x - 5 + 'px'
            } else {
                selectionDiv.style.left = selection.end.x + 5 + 'px'
            }


            if (selection.start.y < selection.end.y) {
                selectionDiv.style.top = selection.start.y - 5 + 'px'
            } else {
                selectionDiv.style.top = selection.end.y + 5 + 'px'
            }



            selectionDiv.style.width = Math.abs(selection.end.x - selection.start.x) - 5 + 'px'
            // selectionDiv.style.width = selection.start.x - selection.end.x + 'px'
            selectionDiv.style.height = Math.abs(selection.end.y - selection.start.y) - 5 + 'px'
        }

        const convertSelectionToGridCoordinated = (selection) => {
            // n - Math.floor(n)

            const sortedSelection = {
                start: {x: null, y: null},
                end: {x: null, y: null},
            }
            if (selection.start.x < selection.end.x) {
                sortedSelection.start.x = selection.start.x
                sortedSelection.end.x = selection.end.x
            } else {
                sortedSelection.start.x = selection.end.x
                sortedSelection.end.x = selection.start.x
            }
            if (selection.start.y < selection.end.y) {
                sortedSelection.start.y = selection.start.y
                sortedSelection.end.y = selection.end.y
            } else {
                sortedSelection.start.y = selection.end.y
                sortedSelection.end.y = selection.start.y
            }

            const realStartX = this.convertXToReal(sortedSelection.start.x);
            const realStartY = this.convertYToReal(sortedSelection.start.y);
            const realEndX = this.convertXToReal(sortedSelection.end.x);
            const realEndY = this.convertYToReal(sortedSelection.end.y);
            if (realEndX - realStartX < 32 || realEndY - realStartY < 32) {
                console.log('too small selection');
                return
            }


            let endX = realEndX / 64;
            endX = (endX - Math.floor(endX)) > 0.5 ? Math.floor(endX) : Math.floor(endX) - 1
            let endY = realEndY / 64;
            endY = (endY - Math.floor(endY)) > 0.5 ? Math.floor(endY) : Math.floor(endY) - 1

            const startX = Math.round(realStartX / 64);
            const startY = Math.round(realStartY / 64);
            console.log(
                'start x', startX,
                'start y', startY,
                'end x', endX,
                'end y', endY,
            );

            State.selectFieldsObjects({x: startX, y: startY}, {x: endX, y: endY})
        }
    }
}