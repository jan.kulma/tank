import {Assets, Sprite} from 'pixi.js'
import {Building} from './building.js';
import State from './state.js';
import MetalPlant from './metalPlant.js';
import OilPlant from './oilPlant.js';
import PowerPlant from './powerPlant.js';
import TankFactory from './tankFactory.js';

export class Builder extends EventTarget {

    boundMouseMoveHandler = this.mouseMoveHandler.bind(this)
    boundDoBuild = this.doBuild.bind(this)
    boundContextMenuHandler = this.contextMenuHandler.bind(this)

    constructor(grid, resourcesManager) {
        super()

        this.grid = grid
        this.resourcesManager = resourcesManager
        this.addEventListener('build', this.onBuild)
    }

    async onBuild(e) {
        if (this.current) {
            this.current.destroy()
            this.cleanUp()
        }

        const buildingData = e.detail.buildingData;

        const building = this.createBuilding(buildingData)

        this.current = building

        this.grid.addChild(building)

        this.grid.on('click', this.boundDoBuild)
        this.grid.on('mousemove', this.boundMouseMoveHandler)
        document.body.addEventListener('contextmenu', this.boundContextMenuHandler)
    }

    mouseMoveHandler(e) {
        const x = this.grid.convertXToRealRounded(e.global.x)
        const y = this.grid.convertYToRealRounded(e.global.y)
        if (!this.current.visible) {
            this.current.visible = true
        }

        this.current.position.set(x, y)
    }

    doBuild(e) {
        if (!this.resourcesManager.canAfford(this.current.cost)) {
            console.log('cant afford');
            return
        }

        const x = this.grid.convertX(e.global.x)
        const y = this.grid.convertY(e.global.y)
        if (!State.isFieldFree(x, y)) {
            console.log('cannot build there, on the field there is', State.getField(x, y));
            return
        }

        const realX = this.grid.convertXToRealRounded(e.global.x)
        const realY = this.grid.convertYToRealRounded(e.global.y)
        this.current.position.set(realX, realY)
        State.putBuildingInField(x, y, this.current)
        this.resourcesManager.pay(this.current.cost)
        this.current.finishBuilding()
        this.cleanUp()
    }

    contextMenuHandler(e) {
        e.preventDefault()
        if (this.current) {
            this.current.destroy()
            this.cleanUp()
        }
    }

    createBuilding(buildingData) {
        switch (buildingData.id) {
            case MetalPlant.ID:
                return new MetalPlant(buildingData);
            case OilPlant.ID:
                return new OilPlant(buildingData);
            case PowerPlant.ID:
                return new PowerPlant(buildingData);
            case TankFactory.ID:
                return new TankFactory(buildingData);
        }
    }

    cleanUp() {
        this.grid.off('mousemove', this.boundMouseMoveHandler)
        this.grid.off('click', this.boundDoBuild)
        document.body.removeEventListener('contextmenu', this.boundContextMenuHandler)
        this.current = null
    }
}