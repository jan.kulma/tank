import {Building} from './building.js';

export default class TankFactory extends Building {
    static ID = 'tank_factory'

    constructor(buildingData) {
        super(buildingData.id)

        this.cost = {
            metal: 2000
        }
    }

    actions() {
        return [
            {
                name: 'Build Tank',
                handler: 'buildTankHandler'
            }
        ]
    }

    buildTankHandler() {
        console.log('buildTankHandler');
    }
}