import {Assets} from 'pixi.js';

export class AssetsManager {
    static textures

    static async init() {
        Assets.addBundle('allTextures', {
            metal_plant: 'metal_plant.png',
            oil_plant: 'oil_plant.png',
            power_plant: 'power_plant.png',
            tank_factory: 'tank_factory.png',
            tank: 'tank.png',
        });

        this.textures = await Assets.loadBundle('allTextures')
    }
}