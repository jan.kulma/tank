const gridObjectMixin = {
    gridX: null,
    gridY: null,
    setGridXY(x, y) {
        this.gridX = x
        this.gridY = y
    }
}

export default gridObjectMixin