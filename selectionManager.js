import State from './state.js';

export default class SelectionManager {
    constructor(grid, movementManager) {
        this.grid = grid
        this.movementManager = movementManager
        this.selectionEl = this.createAndAddSelectionEl()
        this.selectionContext = []
        this.selection = {
            start: null,
            end: null,
        }

        this.registerSelectionEvents()
    }

    registerSelectionEvents() {
        this.grid.on('mousedown', this.boundMouseDownHandler)
        this.grid.on('rightdown', this.boundRightDownHandler)
        this.grid.on('rightup', this.boundRightUpHandler)
    }

    mouseDownHandler(e) {
        this.selection.start = {
            x: e.global.x,
            y: e.global.y,
        }

        this.grid.on('mousemove', this.boundMouseMoveHandler)
        this.grid.on('mouseup', this.boundMouseUpHandler)
    }

    boundMouseDownHandler = this.mouseDownHandler.bind(this)

    mouseMoveHandler(e) {
        if (Math.abs(this.selection.start.x - e.global.x) < 10 || Math.abs(this.selection.start.y - e.global.y) < 10) {
            console.log('returning from mouse move, selection too small');
            // this.resetSelectionEl()
            // this.selection.end = {
            //     x: null,
            //     y: null,
            // }
            return;
        }

        this.selection.end = {
            x: e.global.x,
            y: e.global.y,
        }
        this.drawSelection()
    }

    boundMouseMoveHandler = this.mouseMoveHandler.bind(this)

    mouseUpHandler(e) {
        if (Math.abs(this.selection.start.x - e.global.x) < 10 || Math.abs(this.selection.start.y - e.global.y) < 10) {
            console.log('returning from mouse up, selection too small');

            this.resetSelectionEl()
            this.grid.off('mousemove', this.boundMouseMoveHandler)
            this.grid.off('mouseup', this.boundMouseUpHandler)
            return;
        }
        const converted = this.convertSelectionToGridCoordinated(this.selection);
        if (!converted) {
            console.log('returning from mouse up, selection too small 2');

            this.resetSelectionEl()
            this.grid.off('mousemove', this.boundMouseMoveHandler)
            this.grid.off('mouseup', this.boundMouseUpHandler)
            return
        }

        const objects = State.getFieldsObjects(converted)
        if (this.selectionContext.length) {
            this.selectionContext.forEach(obj => obj.unselect())
        }
        this.selectionContext = objects
        this.selectionContext.forEach(obj => obj.select())

        this.grid.off('mousedown', this.boundMouseDownHandler)
        this.grid.off('mousemove', this.boundMouseMoveHandler)
        this.grid.off('mouseup', this.boundMouseUpHandler)

        this.resetSelectionEl()

        if (this.selectionContext.length) {
            this.grid.on('mousedown', this.boundSelectDestinationHandler)
        } else {
            this.grid.on('mousedown', this.boundMouseDownHandler)
        }
    }

    boundMouseUpHandler = this.mouseUpHandler.bind(this)

    rightDownHandler(e) {
        console.log('right down')
        this.rightDownPosition = {
            x: e.global.x,
            y: e.global.y,
        }
        // console.log(e);
        // this.selectionContext.forEach(obj => obj.unselect())
        // this.selectionContext = []
        //
        // this.grid.off('mousedown', this.boundSelectTargetHandler)
        // this.grid.on('mousedown', this.boundMouseDownHandler)
    }

    boundRightDownHandler = this.rightDownHandler.bind(this)

    rightUpHandler(e) {
        console.log('right up')
        if (Math.abs(this.rightDownPosition.x - e.global.x) > 10 || Math.abs(this.rightDownPosition.y - e.global.y) > 10) {
            console.log('this was drag');
            return
        }
        this.selectionContext.forEach(obj => obj.unselect())
        this.selectionContext = []

        this.grid.off('mousedown', this.boundSelectDestinationHandler)
        this.grid.on('mousedown', this.boundMouseDownHandler)
    }

    boundRightUpHandler = this.rightUpHandler.bind(this)

    selectDestinationHandler(e) {
        let x = this.grid.convertX(e.x)
        let y = this.grid.convertY(e.y)

        if (this.selectionContext.length < 2) {
            if (!State.isFieldFree(x, y)) {
                console.log('fiend not free, cannot be the destination');
                return
            }

            this.selectionContext.forEach((object) => {
                console.log('creating movements in selection manager');
                object.isMoving = true
                this.movementManager.createMovement(
                    object,
                    {x, y}
                )
            })

            return
        }

        const destBoxSize = Math.ceil(Math.sqrt(this.selectionContext.length))
        for (let i = 0; i < destBoxSize; i++) {
            for (let j = 0; j < destBoxSize; j++) {
                if (!State.isFieldFree(x + i, y + j)) {
                    console.log(`Field ${x + i}, ${y + j} is not free in the destination box`);
                    return
                }
            }
        }


        this.selectionContext.forEach(obj => obj.isMoving = true)

        let addToX = 0;
        let addToY = 0;
        for (let i = 0; i < this.selectionContext.length; i++) {
            const object = this.selectionContext[i]
            this.movementManager.createMovement(
                object,
                {
                    x: x + addToX,
                    y: y + addToY,
                }
            )
            addToX++
            if (i + 1 === destBoxSize) {
                addToX = 0
                addToY++
            }
        }

        // this.selectionContext.forEach((object) => {
        //     console.log('creating movements in selection manager');
        //     this.movementManager.createMovement(
        //         object,
        //         {x, y}
        //     )
        // })
        //
        // for (let i = 0; i < destBoxSize; i++) {
        //     for (let j = 0; j < destBoxSize; j++) {
        //         if (!State.isFieldFree(x + i, y + j)) {
        //             console.log(`Field ${x + i}, ${y + j} is not free in the destination box`);
        //             return
        //         }
        //     }
        // }



        // this.grid.off('mousedown', this.boundSelectTargetHandler)
        // this.grid.on('mousedown', this.boundMouseDownHandler)
    }

    boundSelectDestinationHandler = this.selectDestinationHandler.bind(this)

    drawSelection() {
        this.selectionEl.style.display = 'block'
        if (this.selection.start.x <this. selection.end.x) {
            this.selectionEl.style.left =this. selection.start.x - 5 + 'px'
        } else {
            this.selectionEl.style.left =this. selection.end.x + 5 + 'px'
        }

        if (this.selection.start.y <this. selection.end.y) {
            this.selectionEl.style.top =this. selection.start.y - 5 + 'px'
        } else {
            this.selectionEl.style.top =this. selection.end.y + 5 + 'px'
        }

        this.selectionEl.style.width = Math.abs(this.selection.end.x -this. selection.start.x) - 5 + 'px'
        this.selectionEl.style.height = Math.abs(this.selection.end.y -this. selection.start.y) - 5 + 'px'
    }

    convertSelectionToGridCoordinated() {
        const selection = this.selection

        // sort asc for easier calc
        const sortedSelection = {
            start: {x: null, y: null},
            end: {x: null, y: null},
        }
        if (selection.start.x < selection.end.x) {
            sortedSelection.start.x = selection.start.x
            sortedSelection.end.x = selection.end.x
        } else {
            sortedSelection.start.x = selection.end.x
            sortedSelection.end.x = selection.start.x
        }
        if (selection.start.y < selection.end.y) {
            sortedSelection.start.y = selection.start.y
            sortedSelection.end.y = selection.end.y
        } else {
            sortedSelection.start.y = selection.end.y
            sortedSelection.end.y = selection.start.y
        }

        const realStartX = this.grid.convertXToReal(sortedSelection.start.x);
        const realStartY = this.grid.convertYToReal(sortedSelection.start.y);
        const realEndX = this.grid.convertXToReal(sortedSelection.end.x);
        const realEndY = this.grid.convertYToReal(sortedSelection.end.y);
        if (realEndX - realStartX < 32 || realEndY - realStartY < 32) {
            console.log('too small selection, returning null');
            return null
        }

        let endX = realEndX / 64;
        endX = (endX - Math.floor(endX)) > 0.5 ? Math.floor(endX) : Math.floor(endX) - 1
        let endY = realEndY / 64;
        endY = (endY - Math.floor(endY)) > 0.5 ? Math.floor(endY) : Math.floor(endY) - 1
        const startX = Math.round(realStartX / 64);
        const startY = Math.round(realStartY / 64);

        console.log(
            'start x', startX,
            'start y', startY,
            'end x', endX,
            'end y', endY,
        );

        return {
            start: {
                x: startX,
                y: startY
            },
            end: {
                x: endX,
                y: endY
            }
        }
    }

    createAndAddSelectionEl() {
        const selectionDiv = document.createElement('div')
        selectionDiv.style.display = 'none'
        selectionDiv.classList.add(
            'fixed',
            'z-50',
            'border-2',
            'border-white'
        )
        document.body.appendChild(selectionDiv)

        return selectionDiv
    }

    resetSelectionEl() {
        this.selectionEl.style.display = 'none'
        this.selectionEl.style.left = ''
        this.selectionEl.style.top = ''
        this.selectionEl.style.width = ''
        this.selectionEl.style.height = ''
    }
}