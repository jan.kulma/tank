class State {
    boardWidth = 0
    fields = {}

    buildings = {
        metal_plant: [],
        oil_plant: [],
        power_plant: [],
        tank_factory: []
    }

    movements = []

    metal = 2000
    oil = 0
    power = 0

    startTime = 0

    selectionContext = []

    constructor(options) {
        this.startTime = new Date().getTime() / 1000
        this.boardWidth = options.width
        // for (let i = 0; i < this.boardWidth; i++) {
        //     for (let j = 0; j < this.boardWidth; j++) {
        //         this.fields[`${i}.${j}`] = null
        //     }
        // }
    }

    getField(x, y) {
        return this.fields[x]?.[y] ?? null
    }

    isFieldFree(x, y) {
        if (x < 0 || y < 0) {
            console.log('saying field is not free, but really is out of grid');
            return false
        }
        return this.getField(x, y) === null
    }

    putBuildingInField(x, y, building) {
        this.putInField(x, y, building)
        this.buildings[building.id].push(building)
    }

    removeBuilding(building) {
        this.buildings[building.id] = this.buildings[building.id].filter(item => {
            return item.uid !== building.uid
        })
    }

    putInField(x, y, object) {
        if (!this.fields[x]) {
            this.fields[x] = {}
        }
        this.fields[x][y] = object
    }

    freeUpField(x, y) {
        this.fields[x][y] = null
    }

    getFieldId(x, y) {
        return `${x}.${y}`;
    }

    selectFieldsObjects(start, end) {

        console.log(this.selectionContext);
        this.selectionContext.forEach((object) => {
            object.tint = 0xFFFFFF
        })

        const selected = []
        for (let i = start.x; i <= end.x; i++) {
            if (!this.fields[i]) {
                continue
            }
            for (let j = start.y; j <= end.y; j++) {
                if (!this.fields[i][j]) {
                    continue
                }
                const object = this.fields[i][j];
                object.selected = true
                selected.push(object)
            }
        }

        this.selectionContext = [...selected]
    }

    getFieldsObjects({start, end}) {

        console.log('getting fields objects');

        // console.log(this.selectionContext);
        // this.selectionContext.forEach((object) => {
        //     object.tint = 0xFFFFFF
        // })

        const objects = []
        for (let i = start.x; i <= end.x; i++) {
            if (!this.fields[i]) {
                continue
            }
            for (let j = start.y; j <= end.y; j++) {
                if (!this.fields[i][j]) {
                    continue
                }
                const object = this.fields[i][j];
                object.selected = true
                objects.push(object)
            }
        }

        return objects
    }
}

export default new State({width: 300})