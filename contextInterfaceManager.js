import {Point} from 'pixi.js';
import State from './state.js';

export default class ContextInterfaceManager {

    static menuTemplate = `
        <ul>
            <li class="mb-1"></li>
            <li></li>
            <li></li>
            <li></li>
        </ul>
    `

    constructor(context) {
        this.context = context

        this.context.on('click', this.onClickHandler.bind(this))
    }

    onClickHandler(e) {
        if (this.menuRoot) {
            return
        }

        this.menuRoot = this.buildRootElement();
        this.menuRoot.getElementsByTagName('li')[0].innerText = this.context.id

        const upgradeButton = document.createElement('button')
        upgradeButton.innerText = 'Upgrade'
        upgradeButton.addEventListener('click', this.upgradeButtonClickHandler.bind(this))
        this.menuRoot.getElementsByTagName('li')[1].appendChild(upgradeButton)

        const destroyButton = document.createElement('button')
        destroyButton.innerText = 'Destroy'
        destroyButton.addEventListener('click', this.destroyButtonClickHandler.bind(this))
        this.menuRoot.getElementsByTagName('li')[2].appendChild(destroyButton)

        for (const actionData of this.context.actions()) {
            const actionButton = document.createElement('button')
            actionButton.innerText = actionData.name
            actionButton.addEventListener('click', this.context[actionData.handler])
            this.menuRoot.getElementsByTagName('li')[3].appendChild(actionButton)
        }

        document.body.appendChild(this.menuRoot)

        this.registerCloseListeners()
    }

    buildRootElement() {
        const {height, width, x, y} = this.context.getBounds();
        const div = document.createElement('div');
        div.innerHTML = ContextInterfaceManager.menuTemplate;
        div.style.left = x + width / 2  + 'px';
        div.style.top = y + height / 2 + 'px';
        div.classList.add(
            'fixed',
            'w-24',
            // 'h-12',
            'z-50',
            'bg-black',
            'bg-opacity-50',
            'text-white',
            'font-bold',
            'text-xs',
            'font-mono',
        );
        return div;
    }

    upgradeButtonClickHandler() {
        console.log('upgrade', this.context.id);
        this.cleanUp()
    }

    destroyButtonClickHandler() {
        State.removeBuilding(this.context)
        this.context.destroy()
        this.cleanUp()
    }

    onWheel() {
        this.cleanUp()
    }

    boundOnWheel = this.onWheel.bind(this)

    onClickOutside(e) {
        if (!this.menuRoot.contains(e.target)) {
            this.cleanUp()
        }
    }

    boundOnClickOutside = this.onClickOutside.bind(this)

    registerCloseListeners() {
        document.body.addEventListener('wheel', this.boundOnWheel)

        // timeout because click is registered before the element is added
        setTimeout(() => {
            document.body.addEventListener('mousedown', this.boundOnClickOutside)
        }, 200)
    }

    cleanUp() {
        this.menuRoot.remove()
        this.menuRoot = null
        document.body.removeEventListener('wheel', this.boundOnWheel)
        document.body.removeEventListener('mousedown', this.boundOnClickOutside)
    }
}