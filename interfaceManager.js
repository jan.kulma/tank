import State from '/state.js'

export class InterfaceManager extends EventTarget {

    buttons = [
        {
            id: 'metal_plant',
            name: 'Metal Plant',
            image: 'metal_plant.png',
        },
        {
            id: 'oil_plant',
            name: 'Oil Plant',
            image: 'oil_plant.png',
        },
        {
            id: 'power_plant',
            name: 'Power Plant',
            image: 'power_plant.png',
        },
        {
            id: 'tank_factory',
            name: 'Tank Factory',
            image: 'tank_factory.png',
        },
    ]

    constructor(builder) {
        super()

        this.builder = builder
        this.countersRoot = document.querySelectorAll('div#interface > ul')[0]
        this.buttonsRoot = document.querySelectorAll('div#interface > ul')[1]
        this.init()
    }

    init() {
        this.initCounters()

        for (const buttonData of this.buttons) {
            const button = this.buildButton(buttonData)
            this.attachListeners(button, buttonData)
            this.buttonsRoot.appendChild(this.wrap(button))
        }
    }

    initCounters() {
        this.metalCounter = document.createElement('span')
        this.metalCounter.innerText = 'Metal: ' + State.metal
        this.oilCounter = document.createElement('span')
        this.oilCounter.innerText = 'Oil: ' + State.oil
        this.powerCounter = document.createElement('span')
        this.powerCounter.innerText = 'Power: ' + State.power

        for (const el of [this.metalCounter, this.oilCounter, this.powerCounter]) {
            el.classList.add('p-2')
            el.classList.add('text-sm')
            el.classList.add('text-white')
            el.classList.add('font-mono')
        }

        this.countersRoot.append(
            this.wrap(this.metalCounter),
            this.wrap(this.oilCounter),
            this.wrap(this.powerCounter),
        )

        this.updateCountersContinuously()
    }

    updateCountersContinuously() {
        this.metalCounter.innerText = 'Metal: ' + State.metal
        this.oilCounter.innerText = 'Oil: ' + State.oil
        this.powerCounter.innerText = 'Power: ' + State.power

        setTimeout(this.updateCountersContinuously.bind(this), 1000)
    }

    buildButton(buttonData) {
        const button = document.createElement('button')
        button.classList.add(
            'flex',
            'flex-col',
            'items-center',
            'p-2',
            'text-sm',
            'text-white',
            'font-mono',
            'w-16',
        )
        const img = document.createElement('img')
        img.setAttribute('src', buttonData.image)
        const span = document.createElement('span')
        span.innerText = buttonData.name

        button.append(img, span)

        return button
    }

    attachListeners(button, buttonData) {
        button.addEventListener('click', (e) => {
            this.builder.dispatchEvent(new CustomEvent('build', {
                detail: { buildingData: buttonData }
            }))
        })
    }

    wrap(button) {
        const li = document.createElement('li')
        li.appendChild(button)

        return li
    }
}