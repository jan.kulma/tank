import State from './state.js';

// metal plant lvl 1 => 3 units/s
// metal plant lvl 2 => 6 units/s
// metal plant lvl 3 => 9 units/s

export class ResourcesManager {

    canAfford(cost) {
        for (const resource in cost) {
            const price = cost[resource]
            if (State[resource] < price) {
                return false
            }
        }

        return true
    }

    pay(cost) {
        for (const resource in cost) {
            const price = cost[resource]
            State[resource] -= price
        }
    }

    updateResourcesContinuously() {
        let producedPower = 0
        for (const powerPlant of State.buildings.power_plant) {
            producedPower += powerPlant.production
        }
        for (const metalPlant of State.buildings.metal_plant) {
            State.metal += metalPlant.production
            producedPower -= metalPlant.consumption
        }
        for (const oilPlant of State.buildings.oil_plant) {
            State.oil += oilPlant.production
            producedPower -= oilPlant.consumption
        }

        State.power = producedPower

        setTimeout(this.updateResourcesContinuously.bind(this), 1000)
    }
}