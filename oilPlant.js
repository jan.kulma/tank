import {Building} from './building.js';

export default class OilPlant extends Building {
    static ID = 'oil_plant'

    constructor(buildingData) {
        super(buildingData.id)

        this.production = 2
        this.consumption = 4

        this.cost = {
            metal: 1200
        }
    }
}