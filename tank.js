import gridObjectMixin from './gridObjectMixin.js';
import {Sprite} from 'pixi.js';
import {AssetsManager} from './assetsManager.js';


// speed 1 => 0.5 grid field in 1 sec => 1 path elem in 2 sec
// speed 2 => 1 grid field in 1 sec => 1 path elem in 1 sec
// speed 3 => 2 grid field in 1 sec => 1 path elem in 0,5 sec
// speed 4 => 8 grid field in 1 sec => 1 path elem in 0,2 sec


export default class Tank extends Sprite {
    constructor(gridX, gridY) {
        super(AssetsManager.textures['tank'])

        this.gridX = gridX
        this.gridY = gridY

        this.x = gridX * 64
        this.y = gridY * 64
        this.width = 64
        this.height = 64
        // this.pivot.x = this.width / 2;
        // this.pivot.y = this.height / 2;
        // this.tint = 0x234234
        this.interactive = true
        this.speed = 2
        this.isMoving = false
    }

    select () {
        this.tint = 0x00FF00
    }

    unselect () {
        this.tint = 0xFFFFFF
    }
}

Object.assign(Tank.prototype, gridObjectMixin)